global find_word
extern string_equals

section .text

;rdi - null pointer string - key link
;rsi - begining of the map link
;return adress of key , if success
;else returns 0

find_word:
	xor rax,rax
.loop:
	cmp rsi, 0 ;проверяем ссылку 
	je .end ;если конец словаря
	push rdi
	push rsi
	add rsi, 8 ;смещаем указатель на ключ, так как изначально в rsi лежит ссылка след. элемент
	call string_equals
	pop rsi
	pop rdi
	cmp rax, 1 ;если строки равны
	je .success
	mov rsi, [rsi] ; заносим в rsi ссылку на след элемент в словаре
	jmp .loop
.success:
	mov rax, rsi ; заносим в rsi ссылку на найденный ключ
	ret
.end:
	xor rax, rax
	ret
	
