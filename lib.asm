section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global pars_int
global string_copy
global print_err
global print_stdout_string

;Принимает указатель на строку ошибки, печатает ее

print_err:
	mov r9 , 2
	jmp print_string
	
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	syscall
	
 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
 xor rax, rax	; очищаем rax, чтобы в нем гарантированно был нуль
	.loop:
		cmp byte[rdi+rax], 0	;сравниваем символ с нуль-терминатаром
		je .end			;если нуль-терминатор, на выход
		inc rax			;инкрементируем rax для проверки следующего символа
		jmp .loop
	.end:
		ret	

print_stdout_string:
	mov r9, 1


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length	; в rax лежит длина строки	
	mov rdx, rax		; в rdx кладем длину строки
	mov rsi, rdi		; в rsi кладем адрес начала строки	
	mov rdi, r9		; в rdi кладем дескриптор потока файла потока вывода			
	mov rax, 1		; в rax кладем номер сисколла write			
	syscall
	xor rax, rax
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi , 10
	

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, rsp ;помещаем в r9 адрес вершины стека
	mov rax, rdi ;помещаем в rax число, которое нужно перевести
	mov rcx, 10  ;используем rcx в качестве делителя
	dec rsp;
	mov byte[rsp], 0
.loop:
	xor rdx, rdx
	div rcx ; делим rax на 10, в rdx записывается остаток от деления
	add rdx, 0x30 ; переводим остаток в ascii
	dec rsp
	mov byte[rsp], dl ; сохраняем цифру в стек
	test rax, rax ; промеряем число на то,закончилось ли оно
	jnz .loop

	mov rdi, rsp
	push r9
	call print_string
	pop r9
	mov rsp, r9
	ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns .uint
	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi
.uint:
	call print_uint
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

; в rdi - указатель на первую строку, в rsi - на вторую

	call string_length ; считаем длину первой строки
	mov r9 , rax	  ; помещаем ее длину в r9
	xchg rdi, rsi	  ; помещаем в rdi указатель на вторую строку, для того, чтобы посчитать ее длину в string_length
	call string_length ; считаем длину второй строки
	cmp r9, rax	  ; сравниваем длины двух строк , если не равны - на выход, если равны - начинаем сравнивать строки посимвольно
	jne .notequals
	
.loop:
	xor rax, rax	
	mov al, byte[rdi] ; помещаем в младший байт rax символ
	cmp al, byte[rsi] ; сравниваем символ первой строки с символом второй строк
	jne .notequals    ; если не равны, то выходим
	cmp al, 0	 ; так как символы равны, достаточно проверить на нуль-терминатор только один из них
	je .end	         ; если нуль-терминатор, то мы проверили обе строки и они равны - выходим и возвращаем 1
	inc rdi           ; инкрементируем rdi для чтения следующего символа
	inc rsi		 ; аналогично с rsi
	jmp .loop

.notequals:
	xor rax, rax
	ret	
.end:
	mov rax, 1
	ret
	
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, 0 ;syscall read number
	mov rdi, 0 ;stdin file descriptor
	mov rdx, 1 ;длина 
	push 0	  ; резервируем место для чтения
	mov rsi, rsp ; адрес, куда будет считываться символ
	syscall
	pop rax	     ; считываем в rax введенный символ
	ret	
    
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r13 ;пушим calle-saved регистры, чтобы потом восстановить их
	push r14 
	push r15
	mov r13, rdi ;помещаем в r13 адрес начала буфера
	mov r14, rsi ;помещаем в r14 размер буфера 
	xor r15, r15 ;счетчик прочитанных символов

.first_read:
	call read_char
	cmp rax, 0x20 ;проверяем на пробельный символ
	je .first_read
	cmp rax, 0x9  ;проверяем на пробельный символ
	je .first_read
	cmp rax, 0xA  ;проверяем на пробельный символ
	je .first_read
	cmp rax, 0x0  ;если первый же символ - нуль терминатор
	je .end
	
.continue_read:
	cmp r14, r15 ;проверяем, перевалили ли мы за размер буффера
	jbe .overflow
	mov byte[r13+r15], al ;кладем в буффер первый символ
	cmp al, 0x20
	je .end
	cmp al, 0x9
	je .end
	cmp al, 0xA
	je .end
	cmp al, 0x0
	je .end
	inc r15
	cmp r15, r14
	je .overflow
	call read_char 
	jmp .continue_read

.overflow:
	xor rax, rax
	pop r15
	pop r14
	pop r13
	ret
.end:
	mov byte[r13+r15] , 0
	mov rax, r13
	mov rdx, r15
	pop r15
	pop r14
	pop r13
	ret	
		 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor r10, r10			
	mov rcx, 10			; 10 в rcx используем в качестве делителям
	xor rax, rax
	.loop:
		xor r11, r11
		mov r11b, byte[rdi + r10]
		cmp r11b, 0x39		
		ja .end			
		cmp r11b, 0x30			
		jb .end					
		inc r10			
		sub r11b, '0'		
		mul rcx			
		add rax, r11		
		jmp .loop
	.end:
		mov rdx, r10		
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'		
	jne .uint			
	inc rdi				
	call parse_uint			
	test rdx, rdx			
	jz .not_digit			
	neg rax				
	inc rdx				
	ret
	.uint:
		jmp parse_uint		
		ret
	.not_digit:
		xor rax, rax		
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length ; в rax хранится длина строки
	inc rax
	pop rdx
	pop rsi
	pop rdi
	cmp rax , rdx
	jg .less
.loop:
	xor r11, r11
	mov r11b, byte[rdi]
	mov byte[rsi], r11b
	cmp r11b, 0
	je .end
	inc rdi
	inc rsi
	jmp .loop
	
.less:
	xor rax, rax
	
.end:
	ret
	
	
