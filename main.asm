section .text
%include "colon.inc"
%include "lib.inc"

section .rodata
%include "words.inc"
err: db "The size of string is greater then buffer size",0
err2: db "No word",0

section .text

extern find_word


global _start
_start:
	sub rsp,256 ; освобождаем место под буффер
	mov rdi, rsp ; в rdi лежит адрес на начало буфера для функции read_word
	mov rsi, 256 ;длина буфера для функции read_word
	call read_word
	cmp rax, 0 ; проверяем, не слишкам ли большая строка была введена
	je .overflow
	mov rsi , first
	mov rdi, rax
	call find_word
	add rsp, 256 ; отматываем стек в начальное состояние
	cmp rax, 0 ; проверяем , было ли найдено слово
	je .no_word	
	add rax, 8 ; смещаем указатель на ключ 
	push rax ;заносим адрес ключа в стек
	mov rdi, [rsp] ; готовимся к вызову string_length - законсим в rdi адрес ключа
	call string_length
	pop rdi; забираем адрес ключа из стека в rdi - готовимся к вызову print_string
	add rdi, rax ;смещаем указатель на конец ключа
	inc rdi; учитываем нуль-терминатор, теперь rdi хранит ссылку на значение
	call print_stdout_string
	
.end:
	call print_newline
	mov rdi, 0 
	call exit
	

.overflow:
	add rsp, 256
	mov rdi, err
	call print_err
	jmp .end

.no_word:
	mov rdi, err2
	call print_err
	jmp .end
